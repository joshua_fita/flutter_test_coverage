import 'package:flutter_test/flutter_test.dart';
import 'package:test_coverage/business_logic/counter.dart';

void main() {
  group('Counter', () {
    final counter = Counter();
    test('intial value should be 0', () {
      expect(counter.value, 0);
    });
    test('value should be incremented', () {
      counter.increment();
      expect(counter.value, 1);
    });
    test('value should be decremented', () {
      counter.decrement();
      expect(counter.value, 0);
    });
  });
}
